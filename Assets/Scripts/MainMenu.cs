using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour 
{   
    public void GoToNormalGame()
    {
        PlayerPrefs.SetInt("normalMode", 1);
        SceneManager.LoadScene(1);
    }

    public void GoToExpertGame()
    {
        PlayerPrefs.SetInt("normalMode", 0);
        SceneManager.LoadScene(1);
    }
}