using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public enum GameState {Init, Playing, Ended}

public class GameManager : Singleton<GameManager> 
{   
    #region Settings
        [Header("Settings")]
        [SerializeField] int _coinsPerCompletion = 300;
    #endregion

    #region References
        [Header("References")]
        [SerializeField] GameObject watchAdPopup;
    #endregion

    #region State
        [Header("Game State")]
        [SerializeField] int _level = 1;
        [SerializeField] int _coins = 0; 
        [SerializeField] int _levelsPlayedToday = 0;
        [HideInInspector] public GameState gameState = GameState.Init;

        #region Properties
            public int level { get => _level; }
            public int coins { get => _coins; }
            public int coinsPerCompletion { get => _coinsPerCompletion; }
            public int levelsPlayedToday { get => _levelsPlayedToday; }
            public bool hasWatchedAdToday { get; set; }
        #endregion
    #endregion

    #region Events
        [Header("Events"),Space]
        public UnityEvent<int> OnLevelChanged;
        public UnityEvent<int> OnCoinsChanged;
        public UnityEvent<int> OnCoinsIncrease;
    #endregion

    #region Monobehaviour Functions
        protected override void Awake() 
        {
            base.Awake();
            _level = 1;
            _coins = 0;

            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        private void Start() 
        {
            SaveSystem.SaveData data;
            if (SaveSystem.TryLoadState(out data))
            {
                SetLevel(data.level);
                SetCoins(data.coins);

                TimeSpan span = DateTime.Now.Date - DateTime.FromBinary(data.timestamp).ToLocalTime().Date;

                if (span.TotalDays >= 1) 
                {
                    hasWatchedAdToday = false;
                    SetLevelsPlayedCount(0);
                }
                else 
                {
                    hasWatchedAdToday = data.hasWatchedAdToday;
                    SetLevelsPlayedCount(data.levelsPlayedToday);
                    
                    if (!hasWatchedAdToday && _levelsPlayedToday >= 2) watchAdPopup.SetActive(true);
                }
            }
        }
    #endregion

    public void SetLevelsPlayedCount(int amount)
    {
        _levelsPlayedToday = amount;
    }

    public void SetLevel(int level) 
    {
        _level = level;
        OnLevelChanged?.Invoke(level);
    }

    private void AddCoins(int amount)
    {
        _coins += amount;
        OnCoinsIncrease?.Invoke(amount);
        OnCoinsChanged?.Invoke(_coins);
    }

    private void PayCoins(int amount)
    {
        _coins -= amount;
        OnCoinsChanged?.Invoke(_coins);
    }

    public void SetCoins(int coins) 
    {
        _coins = coins;
        OnCoinsChanged?.Invoke(_coins);
    }

    public void ClosePopup()
    {
        hasWatchedAdToday = true;
        watchAdPopup.SetActive(false);
        SaveSystem.SaveState();
    }

    public void ReloadScene() 
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);

        gameState = GameState.Init;
    }

    public void NextLevel()
    {
        _levelsPlayedToday++;
        SetLevel(_level+1);
        AddCoins(_coinsPerCompletion);
        LevelGenerator.Instance.CreateSequence();
        
        SaveSystem.SaveState();
    }

    public void SkipLevel()
    {
        SetLevel(_level+1);
        hasWatchedAdToday = true;
        LevelGenerator.Instance.CreateSequence();
        SaveSystem.SaveState();

        ReloadScene();
    }

    public void GoToMenu()
    {
        // BGM.Instance.DestroyPersistent();
        SceneManager.LoadScene(0);
    }
}