using Newtonsoft.Json;
using System.Security.Cryptography;
using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    [Serializable]
    public class SaveData
    {
        public int level;
        public int coins;
        public int[] sequenceNodes;
        public int[] sequenceDifficulties;

        public bool hasWatchedAdToday;
        public int levelsPlayedToday;
        public long timestamp;

        public void CollectData() {
            level = GameManager.Instance.level;
            coins = GameManager.Instance.coins;
            sequenceNodes = LevelGenerator.Instance.sequence.nodes.ToArray();
            sequenceDifficulties = LevelGenerator.Instance.sequence.difficulties.ToArray();

            hasWatchedAdToday = GameManager.Instance.hasWatchedAdToday;
            levelsPlayedToday = GameManager.Instance.levelsPlayedToday;
            
            if (_saveData != null && (DateTime.Now.Date - DateTime.FromBinary(_saveData.timestamp).ToLocalTime().Date).TotalDays < 1)
                timestamp = _saveData.timestamp;
            else
                timestamp = DateTime.UtcNow.ToBinary();
        }
    }

    private static SaveData _saveData;

    #region Encryption & Decryption
        private const string KEY = "kQofs1lWhYACAfjI+vH0tc4OSgc4c4s2mUfyg7vUpTA=";
        private const string IV = "yhPPYaFog3F0fX79lQLp9w==";
        private static void WriteEncryptedData(SaveData data, FileStream stream)
        {
            using Aes aesProvider = Aes.Create();
            aesProvider.Key = Convert.FromBase64String(KEY);
            aesProvider.IV = Convert.FromBase64String(IV);

            using ICryptoTransform cryptoTransform = aesProvider.CreateEncryptor();
            using CryptoStream cryptoStream = new CryptoStream(
                stream,
                cryptoTransform,
                CryptoStreamMode.Write
            );

            // Debug.Log($"Key: {Convert.ToBase64String(aesProvider.Key)}");
            // Debug.Log($"Initialization Vector: {Convert.ToBase64String(aesProvider.IV)}");
            cryptoStream.Write(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(data)));
        }

        private static SaveData ReadEncryptedData(string path)
        {
            byte[] fileBytes = File.ReadAllBytes(path);
            
            using Aes aesProvider = Aes.Create();
            aesProvider.Key = Convert.FromBase64String(KEY);
            aesProvider.IV = Convert.FromBase64String(IV);

            using ICryptoTransform cryptoTransform = aesProvider.CreateDecryptor(
                aesProvider.Key,
                aesProvider.IV
            );

            using MemoryStream decryptionStream = new MemoryStream(fileBytes);
            using CryptoStream cryptoStream = new CryptoStream(
                decryptionStream,
                cryptoTransform,
                CryptoStreamMode.Read
            );

            using StreamReader reader = new StreamReader(cryptoStream);
            string result = reader.ReadToEnd();

            Debug.Log($"Decrypted result (if the following is not legible, then probably Key or IV is wrong): {result}");
            return JsonConvert.DeserializeObject<SaveData>(result);
        }
    #endregion

    public static void SaveState() 
    {
        string path = Application.persistentDataPath + "/save";
        try 
        {
            if (File.Exists(path)) 
            {
                Debug.Log("Data exists. Deleting previous save data and writing new one.");
                File.Delete(path);
            }
            else
            {
                Debug.Log("Writing new save data to local storage.");
            }
            
            using FileStream stream = File.Create(path);

            _saveData = new SaveData();
            _saveData.CollectData();

            WriteEncryptedData(_saveData, stream);
        }
        catch (Exception e)
        {
            Debug.LogError($"Unable to save data due to: {e.Message} {e.StackTrace}");
        }
    }

    public static bool TryLoadState(out SaveData data)
    {
        string path = Application.persistentDataPath + "/save";
        if (!File.Exists(path))
        {
            Debug.Log("File doesn't exist. Cannot load file.");
            data = null;

            return false;
        }

        try
        {
            if (_saveData == null)
                _saveData = ReadEncryptedData(path);

            data = _saveData;
            return true;

            // if (GameManager.Instance != null && GameManager.Instance.level <= _saveData.level)
            // {

            //     GameManager.Instance.SetLevel(_saveData.level);
            //     GameManager.Instance.SetCoins(_saveData.coins);
            //     LevelGenerator.Instance.LoadLevel(_saveData.sequenceNodes, _saveData.sequenceDifficulties);    

            //     TimeSpan span = DateTime.UtcNow - DateTime.FromBinary(_saveData.timestamp);

            //     if (span.TotalHours >= 24) 
            //     {
            //         GameManager.Instance.hasWatchedAdToday = false;
            //         GameManager.Instance.SetLevelsPlayedCount(0);
            //     }
            //     else 
            //     {
            //         GameManager.Instance.hasWatchedAdToday = _saveData.hasWatchedAdToday;
            //         GameManager.Instance.SetLevelsPlayedCount(_saveData.levelsPlayedToday);
            //     }
            // }
            // else if (GameManager.Instance != null)
            // {
            //     return false;
            // }
            
            // return true;
        }
        catch (Exception e)
        {
            Debug.LogError($"ERROR. Could not load save data due to: {e.Message} {e.StackTrace}");
            data = null;

            return false;
        }
    }

    public static int GetCurrentLevel() 
    {
        SaveData data = _saveData;
        if (data != null || TryLoadState(out data)) 
            return data.level;
        else 
            return 1;
    }

    public static int GetCurrentCoins()
    {
        SaveData data = _saveData;
        if (data != null || TryLoadState(out data)) 
            return data.coins;
        else 
            return 0;
    }
}
