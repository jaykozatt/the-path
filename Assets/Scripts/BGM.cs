using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class BGM : StaticInstance<BGM> 
{
    public AudioSource source;

    protected override void Awake() 
    {
        base.Awake();
        source = GetComponent<AudioSource>();
    }

    public void Play() => source.Play();
    public void Pause() => source.Pause();
    public void Stop() => source.Stop();
}