using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BarController : StaticInstance<BarController>
{
    #region Settings
        [Header("Settings")]

        [Tooltip("Turns on the normal control scheme.")]
        public bool normalMode = false;

        [Tooltip("Sets how big is the region where the slider returns zero."), Range(0,.3f)]
        public float sliderDeadzone = .1f;

        [Tooltip("Sets how fast the bar raises or lowers according to the sliders.")]
        public float maxChangeRate = 25f;

        [Tooltip("Sets how much is the bar able to tilt to either direction.")]
        public float maxTiltRange = 30f;

        [Tooltip("Sets how quickly the slider resets to its minimum value.")]
        public float releaseRate = 1f;
    #endregion

    #region References
        [Header("References")]
        public ConfigurableJoint leftRope;
        public ConfigurableJoint rightRope;
        public Slider normalLeftSlider;
        public Slider normalRightSlider;
        public Slider expertLeftSlider;
        public Slider expertRightSlider;
        private SoftJointLimit _jointLimitLeft;
        private SoftJointLimit _jointLimitRight;
        private SoftJointLimit _jointLimitCache;
        
        #region Aliases 
            Slider leftSlider { get => normalMode? normalLeftSlider : expertLeftSlider; }
            Slider rightSlider { get => normalMode? normalRightSlider : expertRightSlider; }
        #endregion
    #endregion

    #region Variables & Switches
        float _currentRateL = 0;
        float _currentRateR = 0;
        float _currentTilt = 0;
        float _maxRopeLength;
        bool _receivingInputL = false;
        bool _receivingInputR = false;
    #endregion

    #region Touch & Pointer Events
        public void OnPointerDownLeftSlider()
        {
            _receivingInputL = true;
        }
        public void OnPointerDownRightSlider()
        {
            _receivingInputR = true;
        }
        public void OnPointerUpLeftSlider()
        {
            _receivingInputL = false;
        }
        public void OnPointerUpRightSlider()
        {
            _receivingInputR = false;
        }
    #endregion

    #region OnValueChanged Events
        public void OnLeftSliderInput() 
        {
            _currentRateL = 
                (Mathf.Abs(leftSlider.value) > sliderDeadzone || normalMode ? leftSlider.value : 0) * 
                maxChangeRate * 
                (!normalMode && leftSlider.value >= 0 ? .5f : 1)
            ;
        }

        public void OnRightSliderInput()
        {
            _currentRateR = 
                (Mathf.Abs(rightSlider.value) > sliderDeadzone || normalMode ? rightSlider.value : 0) * 
                maxChangeRate * 
                (!normalMode && rightSlider.value >= 0 ? .5f : 1)
            ;
        }
    #endregion

    #region MonoBehaviour Functions
        protected override void Awake() 
        {
            base.Awake();

            // Disable the corresponding control scheme according to the game mode selected.
            normalMode = PlayerPrefs.GetInt("normalMode", 1) > 0;
            if (normalMode)
            {
                expertLeftSlider.gameObject.SetActive(false);
                expertRightSlider.gameObject.SetActive(false);
            }
            else
            {
                normalLeftSlider.gameObject.SetActive(false);
                normalRightSlider.gameObject.SetActive(false);
            }

            // Register the Listener event for each respective slider
            leftSlider.onValueChanged.AddListener(delegate {OnLeftSliderInput();});
            rightSlider.onValueChanged.AddListener(delegate {OnRightSliderInput();});
            
            // Cache the structs for local use
            _jointLimitLeft = leftRope.linearLimit;
            _jointLimitRight = rightRope.linearLimit;

            ReadjustRopeLength();
        }

        // Update is called once per frame
        void Update()
        {
            if (normalMode) ProcessNormalInput();
            else ProcessExpertInput();
        }

        public void ReadjustRopeLength()
        {
            // Compute max rope length by comparing distance between bar and anchor
            _maxRopeLength = 
                leftRope.transform.TransformPoint(leftRope.anchor).y - 
                leftRope.connectedBody.transform.TransformPoint(leftRope.connectedAnchor).y
            ;

            // Set the proper rope length
            _jointLimitLeft.limit = _maxRopeLength;
            _jointLimitRight.limit = _maxRopeLength;

            // And assign it back to the joint components
            leftRope.linearLimit = _jointLimitLeft;
            rightRope.linearLimit = _jointLimitRight;
        }

        private void ProcessNormalInput()
        {
            #region Snap back each Slider if no input received
                // Reset the left slider to 0, upon releasing touch input
                if (!_receivingInputL)
                    leftSlider.value = 0;

                // Reset the right slider to 0, upon releasing touch input
                if (!_receivingInputR)
                    rightSlider.value = 0;
            #endregion

            #region Change the Rope Length according to input
                // Cache the linear limit struct, and then change the left rope's length
                _jointLimitLeft.limit = Mathf.Clamp(
                    _jointLimitLeft.limit - _currentRateL * Time.deltaTime,
                    2, _maxRopeLength
                );
                leftRope.linearLimit = _jointLimitLeft;

                // Cache the linear limit struct, and then change the right rope's length
                _jointLimitRight.limit = Mathf.Clamp(
                    _jointLimitRight.limit - _currentRateL * Time.deltaTime,
                    2, _maxRopeLength
                );
                rightRope.linearLimit = _jointLimitRight;
            #endregion

            #region Adjust the tilt of the Bar
                // Tilt the bar according to the right slider input
                _currentTilt = Mathf.Clamp(
                    _currentTilt + _currentRateR * Time.deltaTime,
                    -maxTiltRange, maxTiltRange
                );

                // Do so by shortening and lengthening both the left & right ropes accordingly.
                _jointLimitCache = _jointLimitLeft;
                _jointLimitCache.limit = Mathf.Clamp(
                    _jointLimitCache.limit - _currentTilt,
                    2, _maxRopeLength
                );
                leftRope.linearLimit = _jointLimitCache;

                _jointLimitCache = _jointLimitRight;
                _jointLimitCache.limit = Mathf.Clamp(
                    _jointLimitCache.limit + _currentTilt,
                    2, _maxRopeLength
                );
                rightRope.linearLimit = _jointLimitCache;
            #endregion
        }

        private void ProcessExpertInput() 
        {
            // Reset the left slider to 0, and gradually decrease to min, upon releasing touch input
            if (!_receivingInputL)
                leftSlider.value = Mathf.Clamp(
                    leftSlider.value - releaseRate * Time.deltaTime, 
                    leftSlider.minValue, 0
                );

            // Reset the right slider to 0, and gradually decrease to min, upon releasing touch input
            if (!_receivingInputR)
                rightSlider.value = Mathf.Clamp(
                    rightSlider.value - releaseRate * Time.deltaTime, 
                    rightSlider.minValue, 0
                );

            // Cache the linear limit struct, and then change the left rope's length
            _jointLimitLeft.limit = Mathf.Clamp(
                _jointLimitLeft.limit - _currentRateL * Time.deltaTime,
                2, _maxRopeLength
            );
            leftRope.linearLimit = _jointLimitLeft;

            // Cache the linear limit struct, and then change the right rope's length
            _jointLimitRight.limit = Mathf.Clamp(
                _jointLimitRight.limit - _currentRateR * Time.deltaTime,
                2, _maxRopeLength
            );
            rightRope.linearLimit = _jointLimitRight;
        }

        
    #endregion
}
