using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlane : MonoBehaviour
{
    public GameObject gameOverScreen; 
    public AudioSource source;

    private void OnTriggerEnter(Collider other) 
    {
        if (GameManager.Instance.gameState != GameState.Ended) 
        {
            source.Play();
            Destroy(other.gameObject);    
            gameOverScreen.SetActive(true);
        }
    }
}
