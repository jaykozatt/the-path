using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody rb;
    AudioSource source;
    private void Awake() 
    {
        rb = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionStay(Collision other) 
    {
        // Debug.Log($"Ball's angular velocity: {rb.angularVelocity.sqrMagnitude}");
        source.volume = Mathf.Min(1, rb.angularVelocity.sqrMagnitude / 9);
    }

    private void OnCollisionExit(Collision other) 
    {
        source.volume = 0;
    }
}
