using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class RopeRenderer : MonoBehaviour
{
    #region Settings
        [Header("Settings")]
        public Color color = Color.white;
        public float width = 1f;
    #endregion

    #region References
        [Header("References")]
        public Material material;
        LineRenderer _line;
        ConfigurableJoint _joint;
    #endregion

    #region Variables & Switches
        Vector3[] _points;
    #endregion

    #region Monobehaviour Functions
        private void Awake() 
        {
            if (!TryGetComponent<LineRenderer>(out _line))
                _line = gameObject.AddComponent<LineRenderer>();    
            
            _joint = GetComponent<ConfigurableJoint>();

            _points = new Vector3[2];    
        }

        void Start()
        {
            _line.startColor = color;
            _line.endColor = color;

            _line.startWidth = width;
            _line.endWidth = width;

            _line.sharedMaterial = material;
        }

        void Update()
        {
            // Get the 2 extremes of the rope in World Space coordinates
            _points[0] = transform.TransformPoint(_joint.anchor);
            _points[1] = _joint.connectedBody.transform.TransformPoint(_joint.connectedAnchor); 

            _line.SetPositions(_points);
        }
    #endregion
}
