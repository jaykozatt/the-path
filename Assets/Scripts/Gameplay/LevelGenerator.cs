using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : StaticInstance<LevelGenerator>
{
    #region Data Structures
        [System.Serializable]
        public struct SequenceData {
            public List<int> nodes;
            public List<int> difficulties;

            public SequenceData(int[] nodes, int[] difficulties)
            {
                this.nodes = new List<int>(nodes);
                this.difficulties = new List<int>(difficulties);
            }
        } 
    #endregion

    #region Settings
        [Header("Settings")]

        [Tooltip("Sets the distance in Unity units between each level segment.")]
        public float distanceBetweenPieces = 40;
        
        [Tooltip("Sets how many levels need to pass until there's a difficulty increase.")]
        public int levelsUntilDifficultyIncrease = 5;

        [Tooltip("Sets how many levels need to pass until there's a level length increase.")]
        public int levelsUntilLevelLengthIncrease = 10; 
    #endregion

    #region References
        [Header("References")]
        public Transform goal;
        private List<GameObject>[] _endSegments;
        private List<GameObject>[][] _middleSegments;
        private List<GameObject>[] _startSegments;
        private List<GameObject> _currentLevel;
    #endregion

    #region Variables & Switches
        private SequenceData _sequence;
    #endregion

    #region Properties
        private int middleSegmentCount {get => 2 + GameManager.Instance.level / levelsUntilLevelLengthIncrease;}
        private int maxDifficulty { get => GameManager.Instance.level / levelsUntilDifficultyIncrease; }
        public SequenceData sequence { get => _sequence; }
    #endregion

    #region Monobehaviour Functions
        protected override void Awake() 
        {
            base.Awake();

            _endSegments = new List<GameObject>[4];
            _middleSegments = new List<GameObject>[4][];
            _startSegments = new List<GameObject>[4];

            for(int i = 0; i<4; i++)
            {
                _endSegments[i] = new List<GameObject>();
                _startSegments[i] = new List<GameObject>();

                _middleSegments[i] = new List<GameObject>[4];
                for(int j = 0; j<4; j++)
                {
                    _middleSegments[i][j] = new List<GameObject>();
                }
            }

            _currentLevel = new List<GameObject>();

            _sequence.nodes = new List<int>();
            _sequence.difficulties = new List<int>();
        }

        private void Start() 
        {
            // Load all of the segment prefabs into their corresponding data structures
            for (int i = 0; i<4; i++)
            {
                _startSegments[i].AddRange(Resources.LoadAll<GameObject>($"Prefabs/LevelGen/Start/0{i+1}"));
                _endSegments[i].AddRange(Resources.LoadAll<GameObject>($"Prefabs/LevelGen/End/{i+1}0"));
                
                for(int j=0; j < 4; j++)
                {
                    _middleSegments[i][j].AddRange(Resources.LoadAll<GameObject>($"Prefabs/LevelGen/Middle/{i+1}X/{i+1}{j+1}"));
                }
            }

            // Attempt to load any savedata that belongs to a later level than the current one.
            // if no relevant data is found, then generate a new level.
            SaveSystem.SaveData data;
            if (SaveSystem.TryLoadState(out data) ) 
                LoadLevel(data.sequenceNodes, data.sequenceDifficulties);
            else
                GenerateNewLevel();
        }
    #endregion

    // Generates 2 sequences of numbers.
    // The first determines the position where each segment ends (positions go from 1 to 4 inclusively)
    // The second determines which segment to use from the pool of valid segments for a given position.
    public void CreateSequence() 
    {
        _sequence.nodes.Clear();
        _sequence.difficulties.Clear();
        for(int i=0; i <= middleSegmentCount; i++)
        {
            _sequence.nodes.Add(Random.Range(1,5));
            _sequence.difficulties.Add(Random.Range(0,maxDifficulty+1));
        }

        _sequence.difficulties.Add(Random.Range(0,maxDifficulty+1));
    }

    // Loads the incoming sequence data.
    private void LoadSequence(int[] nodeData, int[] difficultyData)
    {
        _sequence.nodes.Clear();
        _sequence.nodes.AddRange(nodeData);

        _sequence.difficulties.Clear();
        _sequence.difficulties.AddRange(difficultyData);
    }

    // Loads the incoming sequence data, and build a level according to said sequence.
    public void LoadLevel(int[] nodeData, int[] difficultyData)
    {
        LoadSequence(nodeData, difficultyData);
        DisposeOfPreviousLevel();
        BuildLevel();

        print("Loaded Level successfully.");
    }

    // Creates a new sequence for generation, and builds the corresponding level for said sequence.
    public void GenerateNewLevel() 
    {
        CreateSequence();
        DisposeOfPreviousLevel();
        BuildLevel();

        SaveSystem.SaveState();

        print("Generated new Level successfully.");
    }

    // If a level is currently instanced, it disposes of each of its segments.
    public void DisposeOfPreviousLevel() 
    {
        if (_currentLevel.Count > 0)
        {
            foreach(GameObject segment in _currentLevel) Destroy(segment);
            _currentLevel.Clear();
        }
    }

    // Builds a level according to the current sequence in memory.
    public void BuildLevel()
    {
        // Get the position indices where the current segment begins and ends.
        int currentNode = 0;
        int nextNode = _sequence.nodes[0] - 1;

        // Retrieve the difficulty of the next segment to be placed.
        // If the difficulty exceeds the number of available segments, then the hardest segment is used.
        int difficulty = Mathf.Min(
            _sequence.difficulties[0],
            _startSegments[nextNode].Count-1
        );

        // Instance the appropriate startimg segment.
        _currentLevel.Add(
            Instantiate(
                _startSegments[nextNode][difficulty],
                Vector3.forward * 6, Quaternion.identity,
                transform
            )
        );

        // And make sure the PathCreator component creates the road mesh for this segment
        _currentLevel[0].GetComponentInChildren<PathCreation.Examples.RoadMeshCreator>().TriggerUpdate();

        // Finally, set the next positional node as the current node.
        currentNode = nextNode;

        for (int i=0; i < _sequence.nodes.Count-1; i++)
        {
            // Get the position index where the current segment ends and the next begins.
            nextNode = _sequence.nodes[i+1] - 1;

            // Retrieve the difficulty of the next segment to be placed.
            // If the difficulty exceeds the number of available segments, then the hardest segment is used.
            difficulty = Mathf.Min(
                _sequence.difficulties[i+1],
                _middleSegments[currentNode][nextNode].Count-1
            );

            // Instance the corresponding segment.
            _currentLevel.Add(
                Instantiate(
                    _middleSegments[currentNode][nextNode][difficulty],
                    new Vector3(0, distanceBetweenPieces * (i+1), 6), 
                    Quaternion.identity,
                    transform
                )
            );

            // And make sure the PathCreator component creates the road mesh for this segment
            _currentLevel[i+1].GetComponentInChildren<PathCreation.Examples.RoadMeshCreator>().TriggerUpdate();

            // Finally, set the next positional node as the current node.
            currentNode = nextNode;
        }

        // Retrieve the difficulty of the next segment to be placed.
        // If the difficulty exceeds the number of available segments, then the hardest segment is used.
        difficulty = Mathf.Min(
            _sequence.difficulties[_sequence.difficulties.Count-1],
            _endSegments[currentNode].Count-1
        );

        // Finally, instance the segment that ends on the goal.
        _currentLevel.Add(
            Instantiate(
                _endSegments[currentNode][difficulty],
                new Vector3(0, distanceBetweenPieces * (_sequence.nodes.Count), 6), 
                Quaternion.identity,
                transform
            )
        );

        // And make sure the PathCreator component creates the road mesh for this segment
        _currentLevel[_currentLevel.Count-1].GetComponentInChildren<PathCreation.Examples.RoadMeshCreator>().TriggerUpdate();

        // Also, move the goal to the correct height at the end of the sequence.
        goal.localPosition = new Vector3(0, distanceBetweenPieces * (_sequence.nodes.Count+1), 6);

        // And readjust the length of the ropes for the current level.
        BarController.Instance.ReadjustRopeLength();

        // Then, start the game.
        GameManager.Instance.gameState = GameState.Playing;
    }

}
