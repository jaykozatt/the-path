using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public GameObject levelCompleteScreen; 
    public AudioSource source;

    private void OnTriggerEnter(Collider other) 
    {
        source.Play();
        GameManager.Instance.gameState = GameState.Ended;
        GameManager.Instance.NextLevel();

        levelCompleteScreen.SetActive(true);
    }

    
}
