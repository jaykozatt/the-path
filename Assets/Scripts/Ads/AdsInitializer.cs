using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
 
public class AdsInitializer : MonoBehaviour, IUnityAdsInitializationListener
{
    [SerializeField] string _androidGameId;
    [SerializeField] string _iOSGameId;
    [SerializeField] bool _testMode = true;
    private string _gameId;
 
    private RewardedAd[] _adUnits;

    void Start()
    {
        _adUnits = GetComponentsInChildren<RewardedAd>();

        InitializeAds();
    }
 
    public void InitializeAds()
    {
        if (!Advertisement.isInitialized)
        {
            _gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
                ? _iOSGameId
                : _androidGameId;
            Advertisement.Initialize(_gameId, _testMode, this);
        }
        else
        {
            LoadAdUnits();
        }
    }
 
    private void LoadAdUnits()
    {
        for (int i=0; i<_adUnits.Length; i++)
            _adUnits[i].LoadAd();
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
        LoadAdUnits();
    }
 
    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
}