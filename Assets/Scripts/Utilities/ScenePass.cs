using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenePass : MonoBehaviour
{
    public void MovoToScene(int sceneID)
    {
        SceneManager.LoadScene(sceneID);
    }

}