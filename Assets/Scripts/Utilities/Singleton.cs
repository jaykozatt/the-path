using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StaticInstance<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance { get; protected set; }
    protected virtual void Awake() => Instance = this as T;

    protected virtual void OnApplicationQuit() 
    {
        Destroy(gameObject);
        Instance = null;
    }
}

public abstract class Singleton<T> : StaticInstance<T> where T : MonoBehaviour
{
    protected override void Awake()
    {
        if (Instance != null) Destroy(gameObject);
        base.Awake();
    }
}

public abstract class PersistentSingleton<T> : Singleton<T> where T : MonoBehaviour
{
    protected override void Awake()
    {
        Debug.Log(Instance);
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    public void DestroyPersistent()
    {
        Instance = null;
        Destroy(this.gameObject);
    }
}

