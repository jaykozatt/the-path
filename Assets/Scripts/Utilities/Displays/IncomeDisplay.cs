using UnityEngine;

public class IncomeDisplay : Display 
{

    private void Start() 
    {
        UpdateText(GameManager.Instance.coinsPerCompletion);
        GameManager.Instance.OnCoinsIncrease.AddListener(UpdateText);
    }    

    public override void UpdateText(int text) 
    {
        UpdateText("(+" + text.ToString() + "     )");
    }
    
}