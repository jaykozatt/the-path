using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Display : MonoBehaviour
{
    protected TextMeshProUGUI _textDisplay;

    protected void Awake() 
    {
        _textDisplay = GetComponent<TextMeshProUGUI>();    
    }

    public virtual void UpdateText(string newText)
    {
        try {
            _textDisplay.text = newText;
        }
        catch 
        {
            Debug.LogError("The textDisplay reference was null. Likely this game object was inactive and didn't initialise.");
        }

    }
    public virtual void UpdateText(int newText) => UpdateText(newText.ToString());
}
