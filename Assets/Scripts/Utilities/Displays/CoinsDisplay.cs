using UnityEngine;

public class CoinsDisplay : Display 
{
    private void Start() 
    {
        UpdateText(SaveSystem.GetCurrentCoins());
        GameManager.Instance?.OnCoinsChanged.AddListener(UpdateText);
    }
}