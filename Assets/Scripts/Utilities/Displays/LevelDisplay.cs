using UnityEngine;

public class LevelDisplay : Display 
{
    private void Start() 
    {
        UpdateText(SaveSystem.GetCurrentLevel());
        GameManager.Instance?.OnLevelChanged.AddListener(UpdateText);
    }    

    
}