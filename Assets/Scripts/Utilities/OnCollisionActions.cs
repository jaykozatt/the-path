using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnCollisionActions : MonoBehaviour
{
    [Header("On Collision Responses"),Space]
    public UnityEvent onCollisionEnter;
    public UnityEvent onCollisionExit;
    public UnityEvent onCollisionStay;

    [Header("On Trigger Responses"),Space]
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;
    public UnityEvent onTriggerStay;

    private void OnCollisionEnter(Collision other) {
        Debug.Log($"{gameObject.name}: Received a collision event");
        onCollisionEnter.Invoke();
    }
    private void OnCollisionExit(Collision other) {
        onCollisionExit.Invoke();
    }
    private void OnCollisionStay(Collision other) {
        onCollisionStay.Invoke();
    }

    private void OnTriggerEnter(Collider other) {
        onTriggerEnter.Invoke();
    }
    private void OnTriggerExit(Collider other) {
        onTriggerExit.Invoke();
    }
    private void OnTriggerStay(Collider other) {
        onTriggerStay.Invoke();
    }
}
